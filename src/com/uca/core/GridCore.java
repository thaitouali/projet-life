package com.uca.core;

import com.uca.core.CellCore;
import com.uca.dao.*;
import com.uca.entity.*;

import java.net.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.regex.*;

public class GridCore {

    /* mets à jour l'état des cellules au tour suivant */
    public static void setNextState(Connection connection) throws SQLException{
        List<CellEntity> g = new CellCore().getLivingCells(connection); // on récupère l'ancienne grille sous forme de List<CellEntity>
        GridEntity grid = new GridEntity(); // ancienne grille
        GridEntity nextGrid = new GridEntity(); // nouvelle grille

        // insertion des éléments de l'ancienne grille dans une GridEntity
        for(CellEntity c : g){
            grid.addCell(c);
        }

        int nbCellVivantes;
        Boolean isAlive;

        // calcul de l'état suivant 
        for (int x = 0; x < 100; x++){
            for (int y = 0; y < 100; y++){
                nbCellVivantes = 0; // nb cellules voisines qui sont vivantes
                isAlive = false; // la cellule est-elle vivante ?
                for (CellEntity c : grid.getCells()){
                    if ((c.getX() == x && c.getY() == y)){
                        isAlive = true;
                    } else if ((c.getX() == x-1 || c.getX() == x+1 || c.getX() == x) && (c.getY() == y-1 || c.getY() == y+1 || c.getY() == y)){
                        // calcule le nb de cellules vivantes parmi les voisins 
                        nbCellVivantes += 1;
                    }
                }
                if ((nbCellVivantes == 2 || nbCellVivantes == 3) && isAlive){
                    nextGrid.addCell(new CellEntity(x,y));
                } else if (nbCellVivantes == 3 && !isAlive){
                    nextGrid.addCell(new CellEntity(x,y));
                }
            }
        }
        new GridDAO().setNextState(nextGrid, connection);
    }

    /* vide la grille */
    public static String emptyGrid(Connection connection){
        return new GridDAO().emptyGrid(connection);
    }

    /* importe le fichier RLE sur la grille */
    public static String importRLE(String url, Connection connection){
        // On tue toutes les cellules
        String resultat = new GridDAO().emptyGrid(connection);
        if (!resultat.equals("Grille vidée avec succès !")){
            return resultat;
        }
        GridEntity grid = new GridEntity();
        try{
            for (CellEntity c : decodeRLEUrl(url)){
                grid.addCell(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Erreur lors du chargement du RLE" + e.getMessage();
        }
        return new GridDAO().setNextState(grid, connection);
    }

    /**
     * Décode le contenu d'un fichier RLE sous forme de cases à partir d'un URL
     * @param url - url d'un fichier RLE, ex : https://www.conwaylife.com/patterns/glider.rle
     */
    private static List<CellEntity> decodeRLEUrl(String url) throws Exception {
        URL u = new URL(url);
        BufferedReader in = new BufferedReader(
        new InputStreamReader(u.openStream()));

        StringBuffer sb = new StringBuffer();
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            sb.append(inputLine);
            sb.append("\n");
        }
        
        in.close();

        return decodeRLE(sb.toString());
    }
    
    /**
     * Décode le contenu d'un fichier RLE sous forme de cases
     * @param rle - un chaîne représentant une serialisation RLE
     */
    public static List<CellEntity> decodeRLE(String rle) {
        List<CellEntity> cells = new ArrayList<>();
        boolean ignore = false;
        int step = 1;
        int x = 50;
        int y = 50;
        String number;
        Pattern pattern = Pattern.compile("^[0-9]+");
        int i = -1; 
        while (i < rle.length() - 1) {
            i++;
            if (ignore) {
                if (rle.charAt(i) == '\n') {
                    ignore = false;
                }
                continue;
            }
            switch (rle.charAt(i)) {
            case '#':
            case 'x':
            case '!':
                ignore = true;
                continue;
            case '$':
                x = 50;
                y += step;
                step = 1;
                continue;
            case 'b':
                x += step;
                step = 1;
                continue;
            case 'o':
                for (int j = 0; j < step; j++) {
                    CellEntity c = new CellEntity(x++, y);
                    cells.add(c);
                }
                step = 1;
                continue;
            }
            Matcher matcher = pattern.matcher(rle.substring(i));
            if (matcher.find()) {
                number = matcher.group();
                step = Integer.parseInt(number);
                i += number.length() - 1;
            }
        }
        return cells;
    }
}
