package com.uca.core;

import java.util.*;
import java.sql.*;

import com.uca.entity.CellEntity;

import com.uca.dao.CellDAO;

public class CellCore {

    /* récupère les cellules vivantes */
    public List<CellEntity> getLivingCells(Connection connection) throws SQLException{
        return new CellDAO().getLivingCells(connection);
    }

    /* change l'état d'une cellule */
    public String changeState(CellEntity selectedCell, Connection connection){
        return new CellDAO().changeState(selectedCell, connection);
    }
}
