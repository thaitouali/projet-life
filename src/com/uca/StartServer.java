package com.uca;

import com.uca.dao.*;
import com.uca.gui.*;

import java.sql.*;
import java.util.*;

import com.uca.core.GridCore;
import com.uca.core.CellCore;
import com.uca.entity.*;

import com.google.gson.Gson;

import static spark.Spark.*;
import spark.*;

public class StartServer {
    public static void main(String[] args) {
        //Configuration de Spark
        staticFiles.location("/static/");
        port(8081);

        // Création de la base de données, si besoin
        _Initializer.Init();

        /**
         * Définition des routes
         */

        // index de l'application
        get("/", (req, res) -> {
            return IndexGUI.getIndex();
        });

        // retourne l'état de la grille
        get("/grid", (req, res) -> {
            Connection connection = _Connector.getConnection(getSession(req));
            res.type("application/json");
            return new Gson().toJson(new CellCore().getLivingCells(connection));
        });

        // inverse l'état d'une cellule
        put("/grid/change", (req, res) -> {
            Connection connection = _Connector.getConnection(getSession(req));
            Gson gson = new Gson();
            CellEntity selectedCell = (CellEntity) gson.fromJson(req.body(), CellEntity.class);
            
            return new CellCore().changeState(selectedCell, connection);
        });

        // sauvegarde les modifications de la grille 
        post("/grid/save", (req, res) -> {
            Connection connection = _Connector.getConnection(getSession(req));
            return new GridDAO().saveGrid(connection);
        });
 
 
        // annule les modifications de la grille 
        post("/grid/cancel", (req, res) -> {
            Connection connection = _Connector.getConnection(getSession(req));
            return new GridDAO().cancelGrid(connection);
        });

        // charge un fichier rle depuis un URL
        put("/grid/rle", (req, res) -> {
            Connection connection = _Connector.getConnection(getSession(req));
            return GridCore.importRLE(req.body(), connection);
            
        });

        // vide la grille
        post("/grid/empty", (req, res) -> {
            Connection connection = _Connector.getConnection(getSession(req));
            return GridCore.emptyGrid(connection);
        });

        // met à jour la grille en la remplaçant par la génération suivante
        post("/grid/next", (req, res) -> {
            Connection connection = _Connector.getConnection(getSession(req));
            GridCore.setNextState(connection);
            return "Etat suivant";
        });

    }

    /**
     * retourne le numéro de session
     * il y a un numéro de session différent pour chaque onglet de navigateur
     * ouvert sur l'application
     */
    public static int getSession(Request req) {
        return Integer.parseInt(req.queryParams("session"));
    }
}
