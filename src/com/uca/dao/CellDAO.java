package com.uca.dao;

import java.sql.*;
import java.util.*;

import com.uca.entity.CellEntity;
import com.uca.entity.GridEntity;
 

public class CellDAO {

    /* récupère la liste des cellules vivantes */

    public static List<CellEntity> getLivingCells(Connection connection) throws SQLException{ 
        List<CellEntity> celluleVivante = new ArrayList<>();
        try {
            PreparedStatement statement;
            statement = connection.prepareStatement("SELECT x, y FROM plateau WHERE etat = 'vivante';"); 
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                int x = resultSet.getInt("x");
                int y = resultSet.getInt("y");

                // On crée une nouvelle cellule avec ces coordonnées
                CellEntity cellule = new CellEntity(x, y);
                // On ajoute la cellule à la liste
                celluleVivante.add(cellule);
            }
        } catch (SQLException e) {
            // En cas d'erreur, annulation de la transaction
            try {
                connection.rollback();
            } catch (SQLException rollbackException) {
                rollbackException.printStackTrace();
            }
    
            // On renvoie un message d'erreur en cas d'exception
            e.printStackTrace();
        }
        // On renvoie la liste des cellules vivantes
        return celluleVivante;
    }
    

    /* permet de changer l'état d'une cellule à l'état inverse */

    public String changeState(CellEntity selectedCell, Connection connection) {
        // On récupère les coordonnées de la cellule sélectionnée
        int x = selectedCell.getX();
        int y = selectedCell.getY();

        PreparedStatement statement;
        try {
            // Début de la transaction

            // On inverse les coordonnées
            statement = connection.prepareStatement("UPDATE plateau SET etat = CASE WHEN etat = 'vivante' THEN 'morte' ELSE 'vivante' END WHERE x = ? AND y = ?;");
            statement.setInt(1, x);
            statement.setInt(2, y);
            statement.executeUpdate();
    
            // Validation de la transaction
            return "Cellule modifiée avec succès.";
        } catch (SQLException e) {
            // En cas d'erreur, annulation de la transaction
            try {
                connection.rollback();
                // On renvoie un message d'erreur en cas d'exception
                e.printStackTrace();
                return "Erreur lors de la modification de la cellule.";
            } catch (SQLException rollbackException) {
                rollbackException.printStackTrace();
                return "Erreur lors de la tentative de rollback.";
            }
        }
    }

}
