package com.uca.dao;
import com.uca.entity.*;
import java.sql.*;
import com.uca.*;
import java.util.*;

public class _Initializer {

    // nom de la table contenant la grille
    final static String TABLE = "plateau";
    // taille de grille
    final static int SIZE = 100;

    // vérifie si une grille existe
    static boolean existe;

    /* cette méthode permet d'initialiser en créant une table pour la grille si elle n'existe pas */
    public static void Init(){
        Connection connection = _Connector.getMainConnection();
        PreparedStatement statement;

        try{
            
            // Définir le niveau d'isolation
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

            // Désactiver le mode de validation automatique de sorte à commit "à la main"
            connection.setAutoCommit(false);

            // opérations sur la BDD

            statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS plateau (x int, y int, etat varchar(50), CONSTRAINT id_point PRIMARY KEY(x,y));"); 
            statement.executeUpdate() ;
            String plateau = "plateau";
            existe = tableExists(connection,  plateau);

            if(existe){
                               
                //Ajouter des cellules mortes initiales sur un carré 100 x 100

                for(int i=0; i<100; i++) {
                    for(int j=0; j<100; j++){
                        statement = connection.prepareStatement("INSERT INTO plateau(x, y, etat) VALUES(?,?,?) ON CONFLICT (x, y) DO NOTHING;");
                        statement.setInt(1, i);
                        statement.setInt(2, j);
                        statement.setString(3, "morte");
                        statement.executeUpdate();
                    }
                }

                // Mettre à jour des cellules vivantes initiales

                statement = connection.prepareStatement("UPDATE plateau SET etat = ? WHERE x = ? AND y = ?;");
                statement.setString(1, "vivante");
                statement.setInt(2, 5);
                statement.setInt(3, 8);
                statement.executeUpdate();
        
                statement = connection.prepareStatement("UPDATE plateau SET etat = ? WHERE x = ? AND y = ?;");
                statement.setString(1, "vivante");
                statement.setInt(2, 5);
                statement.setInt(3, 9);
                statement.executeUpdate();
        
                statement = connection.prepareStatement("UPDATE plateau SET etat = ? WHERE x = ? AND y = ?;");
                statement.setString(1, "vivante");
                statement.setInt(2, 5);
                statement.setInt(3, 10);
                statement.executeUpdate();
                
            }

            // Valider (commit) la transaction
            connection.commit();

            connection.setAutoCommit(true);

        }  catch (SQLException e) {
            // En cas d'erreur, annuler la transaction et afficher l'exception
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
    }
 
    /* teste si une table existe dans la base de données */
     
    private static boolean tableExists(Connection connection, String tableName) throws SQLException {
        DatabaseMetaData meta = connection.getMetaData();
        ResultSet resultSet = meta.getTables(null, null, tableName, new String[]{"TABLE"});
        return resultSet.next();
    }
}
