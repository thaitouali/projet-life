package com.uca.dao;

import java.util.*;
import java.sql.*;

import com.uca.entity.CellEntity;
import com.uca.core.CellCore;
import com.uca.entity.GridEntity;

public class GridDAO {

    /* à l'aide d'un plateau pré-calculé de l'état suivant, cette méhode met à jour pour une connection
     * spécifique, la base de donnée pour l'état suivant du plateau.
     * 
     * ici, GridEntity plateau contient le plateau à l'état suivant à mettre dans la BDD
     */

    public static String setNextState(GridEntity plateau, Connection connection){

        PreparedStatement statement;
        try {
            // Début de la transaction
            connection.setAutoCommit(false);

            // On met toutes les cellules a mortes
            statement = connection.prepareStatement("UPDATE plateau SET etat = 'morte';");
            statement.executeUpdate();

            // On ajout les nouvelles cellules vivantes
            for (CellEntity c : plateau.getCells()){
                statement = connection.prepareStatement("UPDATE plateau SET etat = 'vivante' WHERE x = ? AND y = ?;");
                statement.setInt(1, c.getX());
                statement.setInt(2, c.getY());
                statement.executeUpdate();
            }
            return "Grille modifiée avec succès!";
        } catch (SQLException e) {
            // En cas d'erreur, annulation de la transaction
            try {
                connection.rollback();
                // On renvoie un message d'erreur en cas d'exception
                e.printStackTrace();
                return "Erreur lors de la modification de la cellule.";
            } catch (SQLException rollbackException) {
                rollbackException.printStackTrace();
                return "Erreur lors de la tentative de rollback.";
            }
        }
    }

    /* vide la grille en mettant toutes les cellules à "morte" */

    public String emptyGrid(Connection connection){
        PreparedStatement statement = null;
            try{
                statement = connection.prepareStatement("UPDATE plateau SET etat = 'morte'");
                statement.executeUpdate();
                return "Grille vidée avec succès !";
            } catch (SQLException e){
                try{
                    e.printStackTrace();
                    connection.rollback();
                    return "Erreur lors du vidage de la grille : "+ e.getMessage();
                } catch(SQLException rollBackException){
                    rollBackException.printStackTrace();
                    return "Erreur lors de la tentative de rollback : " + rollBackException.getMessage();
                }
            }
    }

    /* sauvegarde la grille */

    public String saveGrid(Connection connection){
        try  {
            // Validation de la transaction
            connection.commit();
            return "Grille sauvegardée avec succès.";
        } catch (SQLException e) {
            try{
                connection.rollback();
                e.printStackTrace();
                return "Erreur lors de la sauvegarde de la grille : " + e.getMessage();
            } catch(SQLException rollBackException){
                rollBackException.printStackTrace();
                return "Erreur lors de la tentative de rollback : " + rollBackException.getMessage();
            }
        }
    }

    /* annule les modifications faites sur la grille avec rollback */

    public String cancelGrid(Connection connection) throws SQLException {
        try {
            connection.rollback();
            return "Changements annulés avec succès.";
        } catch (SQLException e) {
            e.printStackTrace();
            return "Erreur lors de l'annulation des changements.";
        } 
    }
}
