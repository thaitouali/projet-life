package com.uca.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class _Connector {
    //...........................................................................................................................................
    private static String url = "jdbc:postgresql://localhost/life";
    private static String user = "mamenielle";
    private static String passwd = "1234";

    private static Connection connect;
    private static HashMap<Integer, Connection> userConnections = new HashMap<>();

    public static Connection getMainConnection(){
        if(connect == null){
            connect = getNewConnection();
        }
        return connect;
    }

    private static Connection getNewConnection() {
        Connection c;
        try {
            c = DriverManager.getConnection(url, user, passwd);
        } catch (SQLException e) {
            System.err.println("Erreur en ouvrant une nouvelle connection.");
            throw new RuntimeException(e);
        }
        return c;
    }

    /* méthode pour gérer les différentes sessions */
    public static Connection getConnection(int idSession) throws SQLException{
        Connection connection = userConnections.get(idSession);

        if (connection == null) {
            // Si aucune connexion n'est associée à la session, créer une nouvelle connexion
            connection = _Connector.getNewConnection();
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED); // niveau d'isolation READ COMMITED
            connection.setAutoCommit(false);
            userConnections.put(idSession, connection);
        }

        return connection;
    }
}
