# Compte Rendu 

## I - Initialisation du plateau

### A - Introduction de la table

On va chercher à implémenter la table pour le plateau, à l'aide de l'instruction ci-dessous : 
```sql
CREATE TABLE IF NOT EXISTS plateau (
    x INT,
    y INT,
    etat VARCHAR(50),
    CONSTRAINT id_point PRIMARY KEY (x, y)
);
```

Chaque ligne de la table correspond alors à une case de coordonnées (`x`, `y`). Son état est `"vivante"` ou `"morte"`. 

On a choisi comme clé primaire le couple (`x`, `y`) qui est unique (on ne peut pas superposer deux cases et une case est identifiée par ses coordonnées).
Pour cela, on utilise les mot clé `CONSTRAINT` avec `PRIMARY KEY`. L'identifiant `id_point` sera la combinaison des colonnes `x` et `y`.

### B - Transaction effectuée

On nous demande d'interagir avec la base de données en créant des transactions, et d'utiliser `setAutoCommit`, `commit` et `rollback` de l’objet Connection. On doit aussi configurer le niveau d'isolation avec la méthode setTransactionIsolation.

Pour cela, après s'être connectés, on structure notre transaction comme ci-dessous, ainsi on s'assure que notre grille sera correctement crée : 
1. Définir le niveau d'isolation à `SERIALIZABLE`.
2. Désactiver le mode de validation automatique de sorte à commit "à la main" avec `connection.setAutoCommit(false);`.
3. Exécuter les actions nécessaires à la création du plateau.
4. Commit.

On précise que l'on utilise un `try-catch` de sorte à récupérer les erreurs et à les afficher en console. Dans le cas où une exception est levée, la transaction est annulée avec `rollback` de sorte à ne pas laisser de traces.

*Note* : On a choisi un niveau d'isolation de `SERIALIZABLE` par défaut. Il n'y aura qu'une seule transaction à la fois de toute manière.

### C - Grille Initiale

On crée un carré de 100 x 100 cellules mortes à l'aide du code ci-dessous (avant de commit) :

```java
for(int i=0; i<100; i++) {
    for(int j=0; j<100; j++){
        statement = connection.prepareStatement("INSERT INTO plateau(x, y, etat) VALUES(?,?,?) ON CONFLICT (x, y) DO NOTHING;");
        statement.setInt(1, i);
        statement.setInt(2, j);
        statement.setString(3, "morte");
        statement.executeUpdate();
    }
}
```

On a précisé `ON CONFLICT (x, y) DO NOTHING` pour éviter les conflits de clés primaires, en ne faisant rien si une autre ligne avec les mêmes valeurs de `x` et `y` existe déjà dans la table.

On a aussi rajouté quelques cellules vivantes par défaut avec :

```java
statement = connection.prepareStatement("UPDATE plateau SET etat = ? WHERE x = ? AND y = ?;");
statement.setString(1, "vivante");
statement.setInt(2, 5);
statement.setInt(3, 8);
statement.executeUpdate();

statement = connection.prepareStatement("UPDATE plateau SET etat = ? WHERE x = ? AND y = ?;");
statement.setString(1, "vivante");
statement.setInt(2, 5);
statement.setInt(3, 9);
statement.executeUpdate();

statement = connection.prepareStatement("UPDATE plateau SET etat = ? WHERE x = ? AND y = ?;");
statement.setString(1, "vivante");
statement.setInt(2, 5);
statement.setInt(3, 10);
statement.executeUpdate();
```

Cela construit une structure très simple de *clignotant*.

## II - Rafraîchir la grille

*Rappel de la consigne : on doit modifier le reste du projet pour retourner les cellules vivantes stockées dans la base de données.*

Pour cela, on a crée une classe du DAO nommée `CellDAO` avec une méthode `getLivingCells()` qui récupère toutes les cellules vivantes en effectuant un `SELECT x, y FROM plateau WHERE etat = 'vivante';`. Cela nous permet de séléctionner uniquement les valeurs de la table qui correspondent à des cellules vivantes.  

On a choisi un niveau d'isolation de `READ COMMITED`. Il permet aux transactions de lire les données récentes tout en évitant les problèmes de lectures impropres.  Dans le cas d'une lecture de table comme on le fait ici, il n'est peut-être pas nécessaire d'utiliser un niveau d'isolation aussi élevé que `SERIALIZABLE`, car on ne modifie pas les données et on ne risque pas de voir des résultats incohérents.

## III - Changer l’état d’une cellule

Pour cette question, on utilise une méthode de `CellDAO`, qui se charge de faire la modification sur la base de donnée. Elle prend en argument une instance de `CellEntity` et retourne un message de succès ou d'erreur.

Elle modifie une cellule à l'aide de la ligne suivante : `UPDATE plateau SET etat = CASE WHEN etat = 'vivante' THEN 'morte' ELSE 'vivante' END WHERE x = ? AND y = ?;`. Cela permet de changer l'état d'une cellule à l'état inverse.

On a aussi ajouté une sécurité : en cas d'erreur sql, on fait un `rollback`.

## IV - Sauvegarder/annuler plusieurs changements d’état

Pour cette question, on a devoir créer différentes connexion. On a donc implémenté une méthode `public static Connection getConnection(int idSession)` dans `_Connector` qui crée une session si besoin. On met le niveau d'isolation à `READ COMMITED` parce qu'on aura besoin plus tard que celle-ci sot à `READ COMMITTED`. De plus, ce niveau d'isolation permet quand même une certaine rigueur puisqu'il évite les lectures impropres. Il n'empêche pas les lectures non-reproductibles, mais cela n'est pas un problème dans notre cas car on a mis des "sécurités" pour les éviter.

En fait, un niveau d'isolation plus élevé comme `SERIALIZABLE` nous empêcherait de pouvoir accéder à la lecture parallèle entre plusieurs sessions après commit, ce qui poserait problème dans l'action de rafraichissement d'une session après que l'autre ait commit des changements.

On désactive aussi l'auto-commit.
En effet, pour chaque opération faite sur le plateau dans une session, on ne commit jamais. On comit uniquement avecle bouton `sauvegarder`, et on `rollback` avec `annuler`. Ainsi, chaque session peut effectuer ds "changements" qui ne sont pas sauvegardés pour aucune session. Cela se sauvegarde que si l'on commit avec `sauvegarder`.

*Note :* On a empêché le cas où deux personnes cherchent à écrire au même endroit. Dans ces cas-là, il faut attendre que la première session ait finie de modifier et sauvegardé (donc on a commit) pour que la deuxième session puisse écrire au même endroit. 

*Note :* De toute manière, si deux sessions écrivent en même temps, il faut penser à rafraîchir afin de prendre en compte les commit de l'autre. Par exemple, si session 1 écrit, et session 2 aussi, alors il faut que la session 1 commit avant que la session 2 écrive sinon la session 2 perd ses mises à jour. Elles ne peuvent pas ête enregistrées.

## V - Importer un fichier RLE à partir d’un URL

Pour cela, on a créé une méthode `public static String importRLE(String url, Connection connection)` qui prend en paramètre l'url souhaitée et une `Connection`(celle de chaque session).

## VI - Réinitialiser l’état de la grille

Pour réinitialiser l’état de la grille, il suffit juste de mettre toutes les cellules de la base à `"morte"` avec la ligne : `statement = connection.prepareStatement("UPDATE plateau SET etat = 'morte'");`. Voir la méthode `emptyGrid`.

## VII - Calculer la génération suivante de la grille

Pour cela, on a tout d'abord une méthode dans `GridCore`, `public static void setNextState(Connection connection)`. Celle-ci récupère la grillle actuelle, ensuite calcule les cellules qui devront être vivantes dans le tour suivant. Puis elle fait appel à `GridDAO` et sa méthode `public static String setNextState(GridEntity plateau, Connection connection)` qui effectue les modifications sur la base de données. D'abord elle vide la grille. Puis elle mets à jour certaines cellules à l'aide de la grille `plateau` passée en entrée et qui donne l'état suivant de la grille. 

## Conclusion

Finallement, on a essayé de gérer au mieux les niveaux d'isolation des transactions de sorte à ce que le jeu de la vie reste jouable sur différentes sessions. On a essayé de gérer les erreurs. On aurait cependant peut-ête pu améliorer la qualité de notre code. 

Mais en globalité, nous avons rempli les conditions de la consigne et le jeu est utilisable.
